import 'dart:io';
import 'dart:math';

class Player {
  String name = '';
  String getName() {
    return name;
  }

  void setName(String name) {
    this.name = name;
  }
}

class Game {
  Player player = Player();
  bool win = false;
  int countP = 0;
  int countans = 0;
  var reset = 'n';

  void showWelcome() {
    print("ยินดีต้อนรับเข้าสู่เกม");
    print("....เดาเลขสุ่มถาม....");
  }

  void showPlayer() {
    print("กรุณาใส่ชื่อของคุณ : ");
    String name = stdin.readLineSync()!;
    player.setName(name);
    print('ยินดีต้อนรับสู่เกม เดาเลขสุ่มถาม : $name !!');
  }

  void guessesNumbe() {
    final random = Random();
    int randNumber = random.nextInt(13);
    int attempt = 0;
    print("คุณสามารถพิมพ์ exit ออกได้ตลอด");
    while (true) {
      attempt += 1;

      print("เลือกตัวเลขที่อยูาระหว่าง 1 ถึง 13: ");
      String chosenNumber = stdin.readLineSync()!;

      if (chosenNumber.toLowerCase() == "exit") {
        print("\nบ๊ายยยยยย");

        break;
      } else if (int.parse(chosenNumber) > 13) {
        print("กรุณาเดาเลขไม่เกิน 13");
        continue;
      }

      if (int.parse(chosenNumber) == randNumber) {
        print("ถูกต้องแล้วคร้าบบ คุณเดาไป $attempt รอบ\n");

        if (randNumber == 0) {
          stdout.write("กรุณาตอบคำถาม");
          stdout.write("อะไรเอ่ยไร้สาระ");
          print("1.คุณ  2.ผู้สร้าง 3.ผู้เล่น 4.เพื่อนผู้สร้าง");
          String ans = stdin.readLineSync()!;
          if (ans == "4") {
            print("คุณตอบถูกกเก่งมากจ้าาา");
            countP++;
            print('คะแนนของคุณ : $countP');
          } else {
            print("ผิดจ้าเล่นใหม่เนาะ...ปะเริ่มใหม่!!!");
            countP--;
            print('คะแนนของคุณ : $countP');
            print("ออกจากเกม พิมพ์ 1 , เล่นต่อพิมพ์ 2");
            String exit = stdin.readLineSync()!;
            if (exit == "1") {
              break;
            } else {
              continue;
            }
          }
        } else if (randNumber == 1) {
          stdout.write("กรุณาตอบคำถาม");
          stdout.write("อะไรเอ่ยสวย");
          print("1.คุณ  2.ผู้สร้าง 3.ผู้เล่น 4.เพื่อนผู้สร้าง");
          String ans = stdin.readLineSync()!;
          if (ans == "2") {
            print("คุณตอบถูกกเก่งมากจ้าาา");
            countP++;
            print('คะแนนของคุณ : $countP');
          } else {
            print("ผิดจ้าเล่นใหม่เนาะ...ปะเริ่มใหม่!!!");
            print("ออกจากเกม พิมพ์ 1 , เล่นต่อ พิมพ์ 2");
            String exit = stdin.readLineSync()!;
            if (exit == "1") {
              break;
            } else {
              continue;
            }
          }
        } else if (randNumber == 2) {
          print(" จงถอดรหัสต่อไปนี้เป็นตัวเลขจำนวน 4 หลัก คำถามคือ ?");
          print("รถไฟไทยสามสีมีนายกฯ");
          print("1.5867 2.5347 3.5348 4.5847");
          String ans = stdin.readLineSync()!;
          if (ans == "1") {
            print("คุณตอบถูกกเก่งมากจ้าาา");
            countP++;
            print('คะแนนของคุณ : $countP');
          } else {
            print("ผิดจ้าเล่นใหม่เนาะ...ปะเริ่มใหม่!!!");
            print("ออกจากเกม พิมพ์ 1 , เล่นต่อ พิมพ์ 2");
            String exit = stdin.readLineSync()!;
            if (exit == "1") {
              break;
            } else {
              continue;
            }
          }
        } else if (randNumber == 3) {
          print(" จงถอดรหัสต่อไปนี้เป็นตัวเลขจำนวน 4 หลัก คำถามคือ ?");
          print("ความหลังในโหล");
          print("1.5867 2.5347 3.5348 4.5847");
          String ans = stdin.readLineSync()!;
          if (ans == "1") {
            print("คุณตอบถูกกเก่งมากจ้าาา");
            countP++;
            print('คะแนนของคุณ : $countP');
          } else {
            print("คุณตอบผิด เริ่มเล่นใหม่น้าาา");
            print("เฉลย.....");
            print(
                "ให้แยกเป็น ความหลัง ใน และ โหลความหลัง มาจากเพลง 16 ปีแห่งความหลัง ของ สุรพล สมบัติเจริญ คือเลข 16 /โหล คือ 12ใน ให้เอา /ความหลัง คือ 16 มาแทรกเข้าข้างใน โหล คือ 12 เป็น 1162รหัสนี้ถอดได้เป็น 1162");
            countP--;
            print('คะแนนของคุณ : $countP');
            print("ออกจากเกม พิมพ์ 1 , เล่นต่อพิมพ์ 2");
            String exit = stdin.readLineSync()!;
            if (exit == "1") {
              break;
            } else {
              continue;
            }
          }
        } else if (randNumber == 4) {
          print(
              "นาฬิกา บอกเวลา 15 นาฬิกา 30 นาที ถามว่า เข็มนาฬิกาทำมุมกันเท่าไร ?");
          print("1.75องศา 2.50องศา 3.45องศา 4.2560องศา");
          String ans = stdin.readLineSync()!;
          if (ans == "1") {
            print("คุณตอบถูกกเก่งมากจ้าาา");
            countP++;
            print('คะแนนของคุณ : $countP');
          } else {
            print("คุณตอบผิด เริ่มเล่นใหม่น้าาา");
            print("ออกจากเกม พิมพ์ 1");
            String exit = stdin.readLineSync()!;
            if (exit == "1") {
              break;
            }
          }
        } else if (randNumber == 5) {
          print(" ความเร็วขแงแสงมีความเร็วอยู่ที่เท่าไร");
          print(
              "1.ประมาณ 500,000 กิโลเมตรต่อนาที่ 2.ประมาณ 400,000 กิโลเมตรต่อนาที่ 3.ประมาณ 300,000 กิโลเมตรต่อนาที่ 4.ประมาณ 200,000 กิโลเมตรต่อนาที่");
          String ans = stdin.readLineSync()!;
          if (ans == "3") {
            print("คุณตอบถูกกเก่งมากจ้าาา");
            countP++;
            print('คะแนนของคุณ : $countP');
          } else {
            print("คุณตอบผิด เริ่มเล่นใหม่น้าาา");
            print("ออกจากเกม พิมพ์ 1");
            String exit = stdin.readLineSync()!;
            if (exit == "1") {
              break;
            }
          }
        } else if (randNumber == 6) {
          print("กระดาษ 1 รีม  มีทั้งหมดกี่แผ่น");
          print("1.380แผ่น 2.420แผ่น 3.460แผ่น 4.480แผ่น");
          String ans = stdin.readLineSync()!;
          if (ans == "4") {
            print("คุณตอบถูกกเก่งมากจ้าาา");
            countP++;
            print('คะแนนของคุณ : $countP');
          } else {
            print("คุณตอบผิด เริ่มเล่นใหม่น้าาา");
            print("ออกจากเกม พิมพ์ 1");
            String exit = stdin.readLineSync()!;
            if (exit == "1") {
              break;
            }
          }
        } else if (randNumber == 7) {
          print("โดยปกติ นักบินจะตัดสินใจลงจอดเมื่ออยู่ในระดับความสูงเท่าใด");
          print(
              "1.60 เมตรเหนือรันเวย์ 2.70 เมตรเหนือรันเวย์ 3.80 เมตรเหนือรันเวย์ 4.50 เมตรเหนือรันเวย์");
          String ans = stdin.readLineSync()!;
          if (ans == "1") {
            print("คุณตอบถูกกเก่งมากจ้าาา");
            countP++;
            print('คะแนนของคุณ : $countP');
          } else {
            print("คุณตอบผิด เริ่มเล่นใหม่น้าาา");
            print("ออกจากเกม พิมพ์ 1");
            String exit = stdin.readLineSync()!;
            if (exit == "1") {
              break;
            }
          }
        } else if (randNumber == 8) {
          print("ผู้สืบราชสมบัติของอังกฤษต้องนับถือศาสนาคริสต์นิการอะไร?");
          print("1.โรมันคาทอลิก 2.ออร์โธด๊อก 3.โปรเตสแตนท์ 4.เพรสไบทีเรียน");
          String ans = stdin.readLineSync()!;
          if (ans == "3") {
            print("คุณตอบถูกกเก่งมากจ้าาา");
            countP++;
            print('คะแนนของคุณ : $countP');
          } else {
            print("คุณตอบผิด เริ่มเล่นใหม่น้าาา");
            print("ออกจากเกม พิมพ์ 1");
            String exit = stdin.readLineSync()!;
            if (exit == "1") {
              break;
            }
          }
        } else if (randNumber == 9) {
          print("ประเทศยูกันดาอยู่ในทวีปอะไร");
          print("1.เอเชีย 2.ยุโรป 3.แอฟริกา 4.ออสเตรเลีย");
          String ans = stdin.readLineSync()!;
          if (ans == "3") {
            print("คุณตอบถูกกเก่งมากจ้าาา");
            countP++;
            print('คะแนนของคุณ : $countP');
          } else {
            print("คุณตอบผิด เริ่มเล่นใหม่น้าาา");
            print("ออกจากเกม พิมพ์ 1");
            String exit = stdin.readLineSync()!;
            if (exit == "1") {
              break;
            }
          }
        } else if (randNumber == 10) {
          print("โพไซดอน เป็นเทพยเจ้าแห่งทะเลของใคร");
          print("1.กรีก 2.โรมัน 3.อิยิป 4.เมโสโปเตเมีย");
          String ans = stdin.readLineSync()!;
          if (ans == "1") {
            print("คุณตอบถูกกเก่งมากจ้าาา");
            countP++;
            print('คะแนนของคุณ : $countP');
          } else {
            print("คุณตอบผิด เริ่มเล่นใหม่น้าาา");
            print("ออกจากเกม พิมพ์ 1");
            String exit = stdin.readLineSync()!;
            if (exit == "1") {
              break;
            }
          }
        } else if (randNumber == 11) {
          print("ใครคือจักรพรรดิองค์สุดท้ายของฝรั่งเศส");
          print(
              "1.จักรพรรดินโปเลียนที่3 2.จักรพรรดินโปเลียนที่2 3.กวินตุส ฟาบิอุส มักซิมุส 4.จักรพรรดินโปเลียนที่5");
          String ans = stdin.readLineSync()!;
          if (ans == "1") {
            print("คุณตอบถูกกเก่งมากจ้าาา");
            countP++;
            print('คะแนนของคุณ : $countP');
          } else {
            print("คุณตอบผิด เริ่มเล่นใหม่น้าาา");
            print("ออกจากเกม พิมพ์ 1");
            String exit = stdin.readLineSync()!;
            if (exit == "1") {
              break;
            }
          }
        } else if (randNumber == 12) {
          print("กีฬากอล์ฟมีกี่หลุม");
          print("1. 10 หลุม 2. 12 หลุม 3. 16 หลุม 4. 18 หลุม");
          String ans = stdin.readLineSync()!;
          if (ans == "4") {
            print("คุณตอบถูกกเก่งมากจ้าาา");
            countP++;
            print('คะแนนของคุณ : $countP');
          } else {
            print("คุณตอบผิด เริ่มเล่นใหม่น้าาา");
            print("ออกจากเกม พิมพ์ 1");
            String exit = stdin.readLineSync()!;
            if (exit == "1") {
              break;
            }
          }
        } else if (randNumber == 13) {
          print("เกล็ดของตัวนิ่มแท้จริงแล้วคือ?");
          print(
              "1.ไว้กันสัตว์อื่นกัด  2.ให้ความอบอุ่นแก่ตัวเอง 3.กระสุนที่เอาไว้ใช้ยิงใส่ศัตรู 4.ป้องกันภัยต่างๆ");
          String ans = stdin.readLineSync()!;
          if (ans == "3") {
            print("คุณตอบถูกกเก่งมากจ้าาา");
            countP++;
            print('คะแนนของคุณ : $countP');
          } else {
            print("คุณตอบผิด เริ่มเล่นใหม่น้าาา");
            print("ออกจากเกม พิมพ์ 1");
            String exit = stdin.readLineSync()!;
            if (exit == "1") {
              break;
            }
          }
        } else if (randNumber == 13) {
          print("เกล็ดของตัวนิ่มแท้จริงแล้วคือ?");
          print(
              "1.ไว้กันสัตว์อื่นกัด  2.ให้ความอบอุ่นแก่ตัวเอง 3.กระสุนที่เอาไว้ใช้ยิงใส่ศัตรู 4.ป้องกันภัยต่างๆ");
          String ans = stdin.readLineSync()!;
          if (ans == "4") {
            print("คุณตอบถูกกเก่งมากจ้าาา");
            countP++;
            print('คะแนนของคุณ : $countP');
          } else {
            print("คุณตอบผิด เริ่มเล่นใหม่น้าาา");
            print("ออกจากเกม พิมพ์ 1");
            String exit = stdin.readLineSync()!;
            if (exit == "1") {
              break;
            }
          }
        } else if (randNumber == 13) {
          print("ประเทศใดขจัดขยะดีที่สุดในโลก");
          print("1.ญี่ปุ่ญ  2.อังกฤษ 3.เนเธอแลนด์ 4.อิตาลี่");
          String ans = stdin.readLineSync()!;
          if (ans == "1") {
            print("คุณตอบถูกกเก่งมากจ้าาา");
            countP++;
            print('คะแนนของคุณ : $countP');
          } else {
            print("คุณตอบผิด เริ่มเล่นใหม่น้าาา");
            print("ออกจากเกม พิมพ์ 1");
            String exit = stdin.readLineSync()!;
            if (exit == "1") {
              break;
            }
          }
        }
      } else if (int.parse(chosenNumber) > randNumber) {
        print("คุณเดาเลขสูงไป");
        continue;
      } else {
        print("คุณเดาเลขต่ำไป");
        continue;
      }
    }
  }
}

void main(List<String> args) {
  Game game = Game();
  game.showWelcome();
  game.showPlayer();
  game.guessesNumbe();
}
